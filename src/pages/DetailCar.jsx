import React from 'react'
import {Card, Button, Row, Col, Container, Accordion} from 'react-bootstrap';
import {FaUserFriends} from 'react-icons/fa';
import {FaCog} from 'react-icons/fa';
import {FaRegCalendar} from 'react-icons/fa';
import { useSelector } from 'react-redux';
import {useParams} from 'react-router-dom';
import '../css/detailcar.css';


const DetailCar = () => {
  const {id} = useParams()
  const {jumlahPenumpang} = useSelector((globalStore) => globalStore.dataReducer);
  const {jenisTransmisi} = useSelector((globalStore) => globalStore.dataReducer);
  const {tahunPembuatan} = useSelector((globalStore) => globalStore.dataReducer);
  const {placeholder} = useSelector((globalStore) => globalStore.tombolReducer);
  const {cars} = useSelector((state) => state.carReducer);
  const car = cars.find((car) => car.id.toString() === id);
  return (
    <>
      <Container>
        <Row>
          <Col md={3}>
            <Card style={{ width: '605px', height: '700px' }} className="detail">
              <Card.Title>Tentang Paket</Card.Title>
              <Card.Subtitle>Include</Card.Subtitle>
              <ul>
                <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
                <li>Sudah termasuk bensin selama 12 jam</li>
                <li>Sudah termasuk Tiket Wisata</li> 
                <li>Sudah termasuk pajak</li>
              </ul>
              <Card.Subtitle>Exclude</Card.Subtitle>
              <ul>
                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                <li>Tidak termasuk akomodasi penginapan</li> 
              </ul>
              <Card.Body >
              <Accordion>
                <Accordion.Item>
                  <Accordion.Header><b>Refund, Reschedule, Overtime</b></Accordion.Header>
                  <Accordion.Body>
                  <ul>
                    <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                    <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                    <li>Tidak termasuk akomodasi penginapan</li> 
                    <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                    <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                    <li>Tidak termasuk akomodasi penginapan</li> 
                    <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                    <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                    <li>Tidak termasuk akomodasi penginapan</li> 
                  </ul>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
              </Card.Body>
            </Card>
            <Button variant="success" className="ton"> {placeholder}</Button>
          </Col>

          <Col md={3}>
            <Card style={{ width: '405px', height: '435px', left: "400px" }} className="detail">
              <Card.Img variant="top" src={car.image} className="image" />
              <Card.Body className="col">
                <Card.Title>{car.name}</Card.Title>
                <Card.Text><FaUserFriends /> {jumlahPenumpang}</Card.Text>
                <Card.Text><FaCog /> {jenisTransmisi}</Card.Text>
                <Card.Text><FaRegCalendar /> {tahunPembuatan}</Card.Text>
                <Card.Text> Total <b className="bold">Rp {car.price}</b></Card.Text>
                <Button variant="success" className="toogle"> {placeholder}</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default DetailCar