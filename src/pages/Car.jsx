import React from 'react';
import {Card, Button, Row, Col, Container} from 'react-bootstrap';
import {FaUserFriends} from 'react-icons/fa';
import {FaCog} from 'react-icons/fa';
import {FaRegCalendar} from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import {useEffect} from 'react';
import {carsAction} from '../redux/action/carAction';

import '../css/car.css'

const Car = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const {cars} = useSelector((state) => state.carReducer);
  console.log(cars);
  const carss = (id) => {
    navigate(`/detailcar/${id}`);
  }
  
  
  const {jumlahPenumpang} = useSelector((globalStore) => globalStore.dataReducer);
  const {jenisTransmisi} = useSelector((globalStore) => globalStore.dataReducer);
  const {tahunPembuatan} = useSelector((globalStore) => globalStore.dataReducer);

  useEffect(() => {
    dispatch(carsAction()) 
  }, [dispatch]);

  return (
    <>
      <Container>
        <Row>
          {cars.map((car) =>
            <Col md={3} key={car.id} className="col">
            <Card style={{ width: '333px', height: '586px' }} className="card">
              <Card.Img variant="top" src={car.image} />
              <Card.Body className="body">
                <Card.Title>{car.name}</Card.Title>
                <Card.Subtitle><b>Rp {car.price} / hari</b></Card.Subtitle>
                <Card.Text>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia dolor laudantium repellendus quas incidunt obcaecati impedit molestiae praesentium odio. 
                </Card.Text>
                <Card.Text><FaUserFriends /> {jumlahPenumpang}</Card.Text>
                <Card.Text><FaCog /> {jenisTransmisi}</Card.Text>
                <Card.Text><FaRegCalendar /> {tahunPembuatan}</Card.Text>
                <Button variant="success" onClick={() => carss(car.id)}>Pilih Mobil</Button>
              </Card.Body>
            </Card>
          </Col>
          )}
        </Row>
      </Container>
    </>
  )
}

export default Car