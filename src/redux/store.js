import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducer';
import {composeWithDevTools} from 'redux-devtools-extension';


const globalStore = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default globalStore