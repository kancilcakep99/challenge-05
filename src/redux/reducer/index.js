import {combineReducers} from 'redux';
import dataReducer from './dataReducer';
import tombolReducer from './tombolReducer';
import carReducer from './carReducer';

const rootReducer = combineReducers({
    dataReducer, tombolReducer, carReducer,
});

export default rootReducer