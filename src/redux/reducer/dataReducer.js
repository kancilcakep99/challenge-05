const initialState = {
    jumlahPenumpang: "4 orang",
    jenisTransmisi: "Manual",
    tahunPembuatan: "Tahun 2020",
}

const dataReducer = (state = initialState, action) => {
    switch(action.type){
        case "SET_DATACAR":
            return {
                ...state,
                jumlahPenumpang: action.payload,
                jenisTransmisi: action.payload,
                tahunPembuatan: action.payload,
            }
            default: {
                return state;
            }
    }
}

export default dataReducer